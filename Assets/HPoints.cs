﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPoints : MonoBehaviour
{
    public float Health;//здоровье
    public float Armor;//броня
    public bool Death;//по дефолту false



    void Update()
    {
        //если вышли за границы то востанавливаем как должно быть
        if (Health > 100)
            Health = 100;
        if (Armor > 100)
            Armor = 100;
        //если здоровье 0 то умер игрок
        if (Health <= 0)
            Death = true;
        if (Armor < 0)
            Armor = 0;
    }



    public void PlayerDamageTake(float PTD)//в PTD записывается урон полученный из других скриптов
    {
        if (Armor > 0)
        {
            float balance;//заводим переменную под делитель урона.
            balance = (Armor + 100) / 100;//сама формула: баланс = (броня(70) + 100) / 100 = 1.7
            PTD = PTD / balance;// урон делим на делитель(баланс)
            Health += PTD;//прибавляем урон к здоровью
            Armor += (PTD / 2) / balance;
        }
        else
        {
            Health += PTD;//прибавляем урон к здоровью
        }
    }
}
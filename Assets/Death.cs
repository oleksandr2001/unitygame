﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{
    private Control player; // U nas est scritp 'Control' k kotoromu prireplen nash personazh.My sozdaem ekzemplar classa etogo scripta
    public Transform start;// object  k kototromy my peremeshchaem nashego personaja pri smerti
    public GameObject Explode;

    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<Control>();//nashemu ekzemplaru prisvaevaetsa object - obrashaemsia k nashemu persu

    }

    // Update is called once per frame
    void Update()
    {

    }

     
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")//esli k objectu,k kotoromu prikreplen etot skript, prikosaetsa chto-to(v nashem skuchae pers) s tagom 'Player'
        {
            player.transform.position = start.position; // perenosim nashego personazha na mesto gde stoit respawn
            Debug.Log("Restart!");
        }

    }
}
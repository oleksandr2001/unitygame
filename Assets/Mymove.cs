﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mymove : MonoBehaviour
{
    public float height; //wysota pryzhka
    public float horSpeed;// skorost peredvizhenia
    private float speed;// - horspeed esli v lewo i + horspeed esli w pravo
    private Rigidbody2D rb;

    private bool isGround; // proveriaet na zem

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //speed pokazuie w jaku storony transformyty
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            speed = -horSpeed;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            speed = horSpeed;
        }

        //Peremeshchaet w prawo ili w lewo na 'speed' edinic po osi x
        transform.Translate(speed, 0, 0);

        speed = 0;

        // jump
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            rb.AddForce(new Vector2(0, height), ForceMode2D.Impulse);
        }



    }
}
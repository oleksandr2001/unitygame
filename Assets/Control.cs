﻿using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{
    public Rigidbody2D RigBud;//suda vstavliaem nashego persa
    public float movespeed; // skorost
    public float jumpheight; // vysota pryzhka
    private bool isGround;// proveriaet na zemle my ili net
    public Transform groundCheck;// suda perenosim groundcheker
    public float groundCheckRadius;// radius proverki zemli groundchecka
    public LayerMask WoIG;//??? proverka layera

    Animator anim; // peremennaja dla obrashchenia k ANimatoru personazha
    float vertical;// dla otslezhivania peredvizhenija v verticalnoi ploskosti

    // Use this for initialization
    void Start()
    {
        RigBud = GetComponent<Rigidbody2D>(); // tut my ego inicializiruem
        anim = GetComponent<Animator>(); // teper cherez anim mozhno obrashchatsia k animatoru persa
    }
    void FixedUpdate()
    {
        isGround = Physics2D.OverlapCircle (groundCheck.position , groundCheckRadius , WoIG );// proveriaet est li w radiuse groundcheckera layer ground
        anim.SetBool("jump",isGround);
        if(!isGround)
        {
            return;
        }
    }
    // Update is called once per frame
    void Update()
    {

        vertical = Input.GetAxis("Vertical");
        
        if(vertical == 0)
        {
            anim.SetBool("wakl", false);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            RigBud.velocity = new Vector2(-movespeed, RigBud.velocity.y); // perenosim persa na '- movespeed' edinic po osi y
            transform.rotation = Quaternion.Euler(0, 180, 0); // rotirovat' personazha v levo
            anim.SetBool("wakl", true);
            Debug.Log("moving left");
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            RigBud.velocity = new Vector2(movespeed, RigBud.velocity.y); // perenosim persa na 'movespeed' edinic po osi y
            transform.rotation = Quaternion.Euler(0, 0, 0); // rotirovat' personazha v pravo(default)
            anim.SetBool("wakl", true);
            Debug.Log("moving right");
        }
        if (isGround && Input.GetKeyDown(KeyCode.Space))
        {
            RigBud.velocity = new Vector2(RigBud.velocity.x, jumpheight); // perenosim persa na 'jumpheight' edinic po osi x
            anim.SetBool("jump",true);
            Debug.Log("Jump");
        }
    }
}

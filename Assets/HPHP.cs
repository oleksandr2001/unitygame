﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPHP : MonoBehaviour
{
    [SerializeField]
    private Image _hpBar;
    public GameObject YouDiedScreen;
    public float MinHp = 0f;
    public float MaxHp = 100f;
    public float CurrentHP;
    public float DamageAmount = 10.0f;
    void Start()
    {
        CurrentHP = MaxHp;
        InvokeRepeating(nameof(TakeDamage), 5.0f, 3.0f);
    }

    public void TakeDamage()
    {
        Debug.Log("LOL");
        CurrentHP -= DamageAmount;
        if (CurrentHP < MinHp)
        {
            CurrentHP = MinHp;      
        }
        float barFillValue = CurrentHP / MaxHp;
        _hpBar.fillAmount = barFillValue;
        if (CurrentHP <= MinHp)
        {
            YouDiedScreen.SetActive(true);
        }
    }

}
